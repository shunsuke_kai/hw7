package p1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.*;
public class body extends HttpServlet{
	protected void doGet(HttpServletRequest req,HttpServletResponse response)throws ServletException,IOException{
		PrintWriter out=response.getWriter();
		URL url =new URL("http://step-test-krispop.appspot.com/peers");
		HttpURLConnection connection =(HttpURLConnection)url.openConnection();
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestMethod("POST");
		BufferedReader br=new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
		String file;
		String contents="";
		while((file=br.readLine())!=null){
			out.println(file);
		}
		br.close();
		connection.disconnect();
		
	}
}
