package p1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class show extends HttpServlet{
	protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		String message=req.getParameter("message");
		PrintWriter out=res.getWriter();
		URL url =new URL("http://step-test-krispop.appspot.com/peers");
		HttpURLConnection connection =(HttpURLConnection)url.openConnection();
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestMethod("POST");
		BufferedReader br=new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
		String file;
		String contents="";
		
		
		
		while((file=br.readLine())!=null){
			if(file.startsWith("http")){
				out.print(file+">> ");
				String word=showOther(file+"/convert",message);
				out.print(word);
			}
		}
		br.close();
		connection.disconnect();
		
		
		
		
		
	}
	
	public String showOther(String file,String message)throws IOException{
		URL url=new URL(file+"?message="+message);
		
		HttpURLConnection cn=(HttpURLConnection)url.openConnection();
		cn.setDoOutput(true);
		cn.setUseCaches(false);
		BufferedReader br=new BufferedReader(new InputStreamReader(cn.getInputStream()));
		String line,contents="";
		
	
		
		
		
		while((line=br.readLine())!=null){
			contents+=line;
			contents+="\n";
		}
		br.close();
		
		return contents;
		
	}
	
	
	
	
	public void showOther_post(String file,HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		
		String message=req.getParameter("message");
		if(message==null) return;		
		URL url=new URL(file);
		HttpURLConnection cn=(HttpURLConnection)url.openConnection();
		cn.setDoOutput(true);
		cn.setUseCaches(false);
		//cn.setRequestMethod("POST");
		PrintWriter pw=new PrintWriter(cn.getOutputStream());
		pw.print("message="+message);
		pw.close();
		
		BufferedReader br=new BufferedReader(new InputStreamReader(cn.getInputStream(),"UTF-8"));
		
		String str;
		String contents="";
		while((str=br.readLine())!=null){
			contents+=str;
		}
		br.close();
		cn.disconnect();
		
		PrintWriter out=res.getWriter();
		out.println(contents);
		
	}

}
