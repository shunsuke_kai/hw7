package p1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class madlib extends HttpServlet{
	Random rand =new Random();
	ArrayList <String>urls=new ArrayList<String>();
	
	
	
	String[] st={"Kai plays <noun> <adverb>","Google runs <adverb>","<name> is <adjective>"};
	protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException{
		URL url =new URL("http://step-test-krispop.appspot.com/peers?endpoint=getword");
		HttpURLConnection connection =(HttpURLConnection)url.openConnection();
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setRequestMethod("POST");
		BufferedReader br=new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
		String line;
		String contents="";
		
		
		
		while((line=br.readLine())!=null){
			if(line.startsWith("http")){
				urls.add(line);
			}
		}
		br.close();
		connection.disconnect();
		PrintWriter out=res.getWriter();
		
		
		String sentence=st[rand.nextInt(st.length)];
		String file=urls.get(rand.nextInt(urls.size()))+"/getword?pos=";
		if(sentence.matches(".*<noun>.*")) sentence=sentence.replaceAll("<noun>",showOther(file,"noun"));
		if(sentence.matches(".*<verb>.*")) sentence=sentence.replaceAll("<verb>",showOther(file,"verb"));
		if(sentence.matches(".*<adjective>.*")) sentence=sentence.replaceAll("<adjective>",showOther(file,"adjective"));
		if(sentence.matches(".*<adverb>.*")) sentence=sentence.replaceAll("<adverb>",showOther(file,"adverb"));
		if(sentence.matches(".*<name>.*")) sentence=sentence.replaceAll("<name>",showOther(file,"name"));
		out.print(sentence);
		
		
		
		
		
		
		
		
		
	}
	
	public String showOther(String file,String message)throws IOException{
		URL url=new URL(file+message);
		
		HttpURLConnection cn=(HttpURLConnection)url.openConnection();
		cn.setDoOutput(true);
		cn.setUseCaches(false);
		BufferedReader br=new BufferedReader(new InputStreamReader(cn.getInputStream()));
		String line,contents="";
		
	
		
		
		
		while((line=br.readLine())!=null){
			contents+=line;
			//contents+="\n";
		}
		br.close();
		
		return contents;
		
	}
}